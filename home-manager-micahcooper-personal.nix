{
  config,
  pkgs,
  lib,
  ...
}:

{
  home.username = "micahcooper";
  home.homeDirectory = "/Users/micahcooper";
  home.stateVersion = "24.05"; # Please read the comment before changing.
  home.sessionVariables = {
    EDITOR = "nvim";
  };

  home.packages = [
    pkgs.alacritty
    pkgs.eza
    pkgs.fishPlugins.forgit
    pkgs.fishPlugins.fzf-fish
    pkgs.fzf
    pkgs.bun
    # pkgs.nomad
    # pkgs.terraform
    pkgs.curl
    pkgs.wget
    pkgs.jq
    pkgs.podman
    pkgs.beam.packages.erlang_27.elixir_1_18
    pkgs.rustc
    pkgs.cargo
    pkgs.zig

    (pkgs.writeShellScriptBin "gelete" ''
      echo "git branch --merged | rg -v main -N | xargs git branch -d"; git branch --merged | rg -v main -N | xargs git branch -d
    '')

  ];

  home.file = {
    ".ssh/config".source = ./.mac-ssh.config;
  };

  programs.ripgrep.enable = true;
  programs.nixvim = import ./nvim.nix { pkgs = pkgs; };
  programs.starship = import ./starship.nix { lib = lib; };

  programs.git = {
    enable = true;
    userName = "mrmicahcooper";
    userEmail = "mrmicahcooper@gmail.com";
    aliases = import ./git.aliases.nix;
    extraConfig = import ./git.extraConfig.nix;
    ignores = [ ];
    signing.key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBCWYl2jPFC/cXwyvhUXFL3QehJjf+481m8rHAMvnz1h";
  };

  programs.fish = {
    enable = true;
    shellAbbrs = import ./shell-aliases.nix;
  };

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
    enableBashIntegration = true;
    enableNushellIntegration = true;
  };

  programs.ssh = {
    extraConfig = ''
      Host *
        IdentityAgent "~/Library/Group Containers/2BUA8C4S2C.com.1password/t/agent.sock"
    '';
  };

  programs.home-manager.enable = true;
}
