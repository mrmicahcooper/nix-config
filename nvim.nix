{ pkgs }:

{
  enable = true;
  defaultEditor = true;
  clipboard.register = "unnamedplus";
  colorschemes.catppuccin = {
    enable = true;
    settings = {
      dimInactive = {
        enabled = true;
        percentage = 0.4;
      };
      # flavour = "macchiato";
      flavour = "latte";
      # flavour = "mocha";
      # flavour = "frappe";
      terminalColors = true;
    };
  };
  globals = {
    mapleader = " ";
    maplocalleader = " ";
    netrw_banner = 0;
    netrw_liststyle = 0;
  };
  opts = {
    autoindent = true;
    backup = false;
    clipboard = "unnamedplus";
    cmdheight = 0;
    conceallevel = 0;
    cursorline = true;
    expandtab = true;
    fileencoding = "utf-8";
    hidden = true;
    hlsearch = false;
    ignorecase = true;
    mouse = "a";
    number = true;
    pumheight = 10;
    scrolloff = 3;
    shiftwidth = 2;
    showmode = false;
    showtabline = 2;
    sidescrolloff = 5;
    softtabstop = 2;
    splitbelow = true;
    splitright = true;
    swapfile = false;
    tabstop = 2;
    termguicolors = true;
    timeoutlen = 1000;
    updatetime = 300;
    whichwrap = "b,s,<,>,[,],h,l";
    wrap = true;
    writebackup = false;
  };
  keymaps = [
    {
      key = "<space>";
      action = "<nop>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<space>";
      action = "<nop>";
      mode = "v";
      options.silent = true;
    }
    {
      key = "<leader>-";
      action = "<cmd>exe 'resize -4' <cr>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>[";
      action = "<cmd>exe 'vertical resize -30' <cr>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>]";
      action = "<cmd>exe 'vertical resize +30' <cr>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>cl";
      action = "<cmd>colorscheme catppuccin-latte<cr>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>cf";
      action = "<cmd>colorscheme catppuccin-frappe<cr>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>co";
      action = "<cmd>colorscheme catppuccin-mocha<cr>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>cm";
      action = "<cmd>colorscheme catppuccin-macchiato<cr>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>=";
      action = "<cmd>exe 'resize +4' <cr>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>t";
      action = "<cmd>w | TestFile<cr>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>T";
      action = "<cmd>w | TestNearest<CR>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>ts";
      action = "<cmd>TestSuite<cr>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>l";
      action = "<cmd>TestLast<cr>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>d";
      action = "<cmd>%DB ~/code/fam/data/fam_test.db<cr>";
      mode = "n";
      options.silent = true;
    }
    {
      key = "<leader>r";
      action = "<cmd>ToggleTerm<cr>";
      mode = "n";
      options.silent = true;
    }
  ];
  plugins = {
    comment.enable = true;
    cmp-nvim-lua.enable = true;
    cmp-nvim-lsp.enable = true;
    web-devicons.enable = true;
    colorizer.enable = true;
    cmp = {
      enable = true;
      autoEnableSources = true;
      settings = {
        sources = [
          { name = "nvim_lsp"; }
          { name = "path"; }
          { name = "buffer"; }
        ];
        mapping = {
          # "<C-Space>" = "cmp.mapping.complete()";
          "<C-d>" = "cmp.mapping.scroll_docs(-4)";
          "<C-e>" = "cmp.mapping.close()";
          "<C-f>" = "cmp.mapping.scroll_docs(4)";
          "<CR>" = "cmp.mapping.confirm({ select = true })";
          "<S-Tab>" = "cmp.mapping(cmp.mapping.select_prev_item(), {'i', 's'})";
          "<Tab>" = "cmp.mapping(cmp.mapping.select_next_item(), {'i', 's'})";
        };
      };
    };
    cmp-treesitter.enable = true;
    treesitter = {
      enable = true;
      settings = {
        highlight.enable = true;
        ensure_installed = "all";
        incrementalSelection.enable = true;
        # grammarPackages = with pkgs.vimPlugins.nvim-treesitter.builtGrammars; [
        #   bash
        #   json
        #   lua
        #   make
        #   markdown
        #   elixir
        #   nix
        #   regex
        #   toml
        #   vim
        #   vimdoc
        #   xml
        #   yaml
        # ];
      };
    };
    lsp = {
      enable = true;
      keymaps.lspBuf = {
        K = "hover";
        gI = "implementation";
        gd = "definition";
        gr = "references";
        "<leader>D" = "type_definition";
        "<leader>rn" = "rename";
      };
      servers = {
        bashls.enable = true;
        cssls.enable = true;
        elixirls.enable = true;
        gopls.enable = true;
        html.enable = true;
        jsonls.enable = true;
        # julials.enable = true;
        lua_ls.enable = true;
        nil_ls.enable = true;
        nixd.enable = true;
        svelte.enable = true;
        tailwindcss.enable = true;
        terraformls.enable = true;
        ts_ls.enable = true;
        vls.enable = true;
        yamlls.enable = true;
        zls.enable = true;
      };
    };
    lsp-format = {
      enable = true;
    };
    telescope = {
      enable = true;
      keymaps = {
        "<leader><space>" = "find_files";
        "<c-f>" = "live_grep";
        "<c-b>" = "buffers";
      };
      extensions = {
        fzf-native.enable = true;
      };
    };
    toggleterm = {
      enable = true;
      settings = {
        autochdir = false;
        direction = "float"; # vertical, horizontal, tab
        hideNumbers = false;
        size = 12;
      };
    };
  };
  extraPlugins = with pkgs.vimPlugins; [
    BufOnly
    diffview-nvim
    gitsigns-nvim
    indent-blankline-nvim-lua
    plenary-nvim
    tabular
    vim-endwise
    vim-eunuch
    vim-fugitive
    vim-projectionist
    vim-rhubarb
    vim-surround
    vim-test
    vim-unimpaired
    vim-dadbod
  ];
  extraConfigVim = builtins.readFile ./custom.vimrc;
}
