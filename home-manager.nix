{ pkgs, lib, ... }:

{
  home.username = "micah";
  home.homeDirectory = "/home/micah";
  home.stateVersion = "23.11"; # Please read the comment before changing.
  home.sessionVariables = {
    EDITOR = "nvim";
  };

  home.file = {
    ".config/hypr/hyprland.conf".source = ./hyprland.conf;
    ".config/hypr/hyprpaper.conf".source = ./hyprpaper.conf;
    ".config/waybar/config.jsonc".source = ./waybar.jsonc;
    ".config/waybar/style.css".source = ./waybar.css;
    ".config/ghostty/config".source = ./ghostty.conf;
    ".config/wlogout".source = ./wlogout;
    ".config/direnv/lib/oprc.sh".source = ./oprc.sh;
    ".config/wofi/".source = builtins.fetchGit {
      url = "https://github.com/quantumfate/wofi.git";
      rev = "6c37e0f65b9af45ebe680e3e0f5131f452747c6f";
    };
    ".config/waybar/catppuccin".source = builtins.fetchGit {
      url = "https://github.com/catppuccin/waybar";
      ref = "main";
      rev = "f74ab1eecf2dcaf22569b396eed53b2b2fbe8aff";
    };
    ".config/cattpuccin-thunderbird".source = builtins.fetchGit {
      url = "https://github.com/catppuccin/thunderbird";
      ref = "main";
      rev = "2c2e9dc5ff622b82cb283a7c41dc4a45f5e1be01";
    };
  };

  home.packages = [
    pkgs.alacritty
    pkgs.eza
    pkgs.fishPlugins.forgit
    pkgs.fishPlugins.fzf-fish
    pkgs.fzf
    pkgs.bun
    pkgs.curl
    pkgs.wget
    pkgs.jq
    pkgs.podman
    pkgs.beam.packages.erlang_27.elixir_1_18
    pkgs.rustc
    pkgs.cargo

  ];

  programs.waybar.enable = true;
  programs.wlogout.enable = true;
  programs.nixvim = import ./nvim.nix { pkgs = pkgs; };
  programs.starship = import ./starship.nix { lib = lib; };
  programs.ripgrep = import ./ripgrep.nix;

  programs.git = {
    enable = true;
    userName = "mrmicahcooper";
    userEmail = "micah+github@mrmicahcooper.com";
    signing.key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBCWYl2jPFC/cXwyvhUXFL3QehJjf+481m8rHAMvnz1h";
    aliases = import ./git.aliases.nix;
    extraConfig = {
      advice.statushints = false;
      branch.autosetuprebase = "always";
      color.ui = "auto";
      diff.algorithm = "patience";
      fetch.prune = true;
      help.autocorrect = 10;
      init.defaultBranch = "main";
      interactive.singlekey = true;
      merge.summary = true;
      push.default = "tracking";
      rebase.autosquash = true;
      gpg = {
        format = "ssh";
        "ssh".program = "/opt/1Password/op-ssh-sign";

      };
      # commit.gpgsign = true;
      core = {
        excludesfile = "~/.csvignore";
        editor = "nvim";
        whitespace = "warn";
      };
    };
  };

  programs.fish = {
    enable = true;
    shellAbbrs = import ./shell-aliases.nix;
  };

  programs.nushell = {
    enable = true;
    shellAliases = import ./shell-aliases.nix;
  };

  programs.wezterm = {
    enable = true;
    extraConfig = builtins.readFile ./wezterm.lua;
  };

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
    enableBashIntegration = true;
    enableNushellIntegration = true;
  };

  services.gpg-agent.enableNushellIntegration = true;
  services.blueman-applet.enable = true;

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
