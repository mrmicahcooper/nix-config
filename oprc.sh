# Copied from https://github.com/venkytv/direnv-op

use_oprc() {
	[[ -f .oprc ]] || return 0
	direnv_load op run --env-file .oprc --no-masking -- direnv dump
}

watch_file .oprc
