{ lib }:

{
  enable = true;
  enableFishIntegration = true;
  settings = {
    format = lib.concatStrings [
      "[╭─󰧂 ](fg:#DA627D)"
      "$nix_shell"
      "$directory"
      "$direnv"
      "$git_branch"
      "$git_status"
      "$c"
      "\${custom.elixir}"
      "$elm"
      "$golang"
      "$haskell"
      "$java"
      "$julia"
      "$bun"
      "$nim"
      "$rust"
      "$fill "
      "$aws"
      "$username"
      "$hostname"
      "$os "
      "$memory_usage"
      "$cmd_duration"
      "$line_break"
      "[╰─](fg:#DA627D)"
      "$shell"
      "$jobs"
      "$character"
    ];

    command_timeout = 200;
    add_newline = false;

    character = {
      success_symbol = "[](bold fg:cyan)";
      error_symbol = "[](bold fg:red)";
    };

    nix_shell = {
      disabled = false;
      format = "[ $state( ($name))](bold blue) ";
      # ❄️
    };

    shell = {
      style = "fg:#DA627D";
      format = "[$indicator ]($style)";
      disabled = false;
      fish_indicator = "󰈺";
      nu_indicator = "";
      zsh_indicator = "󰰸";
      bash_indicator = "";
    };

    fill = {
      symbol = " ";
      style = "fg:bright-white";
    };

    jobs = {
      style = "fg:yellow";
      symbol = "󰑮 ";
      format = "[$symbol$number]($style) ";
    };

    cmd_duration = {
      min_time = 0;
      format = "[󱎫 $duration]($style) ";
    };

    username = {
      show_always = true;
      style_user = "fg:purple";
      style_root = "fg:purple";
      format = "[$user󰓺 ]($style) ";
      disabled = false;
    };

    aws = {
      format = "[$symbol$profile]($style)";
      symbol = " ";
    };

    hostname = {
      ssh_only = false;
      style = "fg:green";
      format = "[󰒍 $ssh_symbol$hostname ]($style)";
      disabled = false;
    };

    directory = {
      format = "[$path ]($style) ";
      truncation_length = 2;
      truncation_symbol = "… /";
      truncate_to_repo = false;
    };

    directory.substitutions = {
      "Documents" = " ";
      "Downloads" = " ";
      "Music" = " ";
      "Pictures" = " ";
    };

    docker_context = {
      symbol = " ";
      style = "";
      format = "[$symbol $context ]($style) $path";
    };

    direnv = {
      disabled = false;
      format = "[$symbol-$loaded-$allowed]($style)";
      # symbol = "direnv ";
      symbol = "󱂀 ";
      style = "fg:green";
      loaded_msg = "loaded  ";
      unloaded_msg = "  ";
      allowed_msg = " ";
      denied_msg = " ";
    };

    time = {
      disabled = true;
      time_format = "%R"; # Hour:Minute Format
      style = "bg:#33658A";
      format = "[[♥ $time ](bg:#33658A)]($style)";
    };

    git_branch = {
      format = "[$symbol$branch ]($style)";
      symbol = " ";
    };

    git_status = {
      format = "[($all_status$ahead_behind) ]($style)";
      ahead = "⇡$count";
      diverged = "⇕⇡$ahead_count⇣$behind_count";
      behind = "⇣$count";
    };

    memory_usage = {
      symbol = "󰤇 ";
      threshold = 0;
      disabled = false;
      format = "$symbol[$ram_pct]($style) ";
    };

    os = {
      disabled = false;
      style = "fg:green";
    };

    os.symbols = {
      Macos = " ";
      Manjaro = " ";
      NixOS = " ";
      Raspbian = " ";
      Ubuntu = " ";
    };

    golang = {
      symbol = " ";
    };
    haskell = {
      symbol = " ";
    };
    hg_branch = {
      symbol = " ";
    };
    java = {
      symbol = " ";
    };
    julia = {
      symbol = " ";
    };
    lua = {
      symbol = " ";
    };
    elixir = {
      symbol = " ";
    };
    nix_shell = {
      symbol = " ";
    };
    nodejs = {
      symbol = " ";
    };
    python = {
      symbol = " ";
    };
    ruby = {
      symbol = " ";
    };
    rust = {
      symbol = " ";
    };
    scala = {
      symbol = " ";
    };
    nim = {
      symbol = " ";
      format = "[$symbol $version]($style)";
    };

    custom.nodejs = {
      shell = [
        "node"
        "--version"
      ];
      symbol = "";
      format = "[$symbol ($output) ]($style)";
      detect_files = [
        "package.json"
        ".node-version"
      ];
      detect_extensions = [
        "js"
        "mjs"
        "cjs"
        "ts"
        "mts"
        "cts"
      ];
      ignore_timeout = true;
    };

    custom.elixir = {
      shell = [
        "rg"
        "-N"
        "elixir "
        ".tool-versions"
        "-r"
        ""
      ];
      symbol = "";
      format = "[$symbol ($output) ]($style)";
      detect_files = [ "mix.exs" ];
    };

  };
}
