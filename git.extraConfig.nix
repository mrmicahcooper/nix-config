{
  advice.statushints = false;
  branch.autosetuprebase = "always";
  color.ui = "auto";
  diff.algorithm = "patience";
  fetch.prune = true;
  help.autocorrect = 10;
  init.defaultBranch = "main";
  interactive.singlekey = true;
  merge.summary = true;
  push.default = "tracking";
  rebase.autosquash = true;
  gpg = {
    format = "ssh";
    "ssh".program = "/Applications/1Password.app/Contents/MacOS/op-ssh-sign";
  };
  commit.gpgsign = true;
  core = {
    excludesfile = "~/.csvignore";
    editor = "nvim";
    whitespace = "warn";
  };
}
