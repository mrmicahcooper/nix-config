{ pkgs, ghostty, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  # Bootloader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/nvme0n1";
  boot.loader.grub.useOSProber = true;

  #networking
  networking.hostName = "clank"; # Define your hostname.
  networking.networkmanager.enable = true;

  time.timeZone = "America/Chicago";
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  # Configure keymap in X11
  services.xserver = {
    xkb = {
      layout = "us";
      variant = "";
    };
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  services.pulseaudio.enable = false;
  security.polkit.enable = true;
  security.rtkit.enable = true;
  # sound.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true;
  services.blueman.enable = true;

  # Allow unfree packages
  # nixpkgs.config.allowUnfree = true;

  users.users.micah = {
    isNormalUser = true;
    description = "Micah Cooper";
    extraGroups = [
      "networkmanager"
      "wheel"
    ];
    packages = with pkgs; [
      firefox
      thunderbird
      spotify
    ];
  };

  environment.systemPackages =  [
    pkgs.curl
    pkgs.dconf
    pkgs.dunst
    pkgs.eza
    pkgs.git
    pkgs.hyprpaper
    pkgs.pamixer
    pkgs.pavucontrol
    pkgs.playerctl
    pkgs.slack
    pkgs.wofi
    pkgs.xdg-desktop-portal-hyprland
    pkgs.xwayland
    pkgs.discord
    pkgs.blueman
    pkgs.zoom-us
    pkgs.nushell
    pkgs.ghostty
  ];

  virtualisation.podman = {
    enable = true;
  };

  users.defaultUserShell = pkgs.fish;
  programs.fish = {
    enable = true;
  };

  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
  };

  programs.waybar.enable = true;
  programs._1password.enable = true;
  programs._1password-gui = {
    enable = true;
    polkitPolicyOwners = [ "micah" ];
  };

  # Some programs need SUID wrappers, can be configured further or are started
  # in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  fonts.packages = with pkgs; [
    noto-fonts
    noto-fonts-cjk-sans
    noto-fonts-emoji
    liberation_ttf
    fira-code
    maple-mono-NF
    fira-code-symbols
    mplus-outline-fonts.githubRelease
    dina-font
    proggyfonts
    nerd-fonts._0xproto
    nerd-fonts.fira-code
    nerd-fonts.fantasque-sans-mono
    nerd-fonts.victor-mono
  ];

  services.openssh.enable = true;

  services.grafana = {
    enable = true;
    settings = {
      "auth.anonymous".enabled = true;
      server = {
        http_port = 4001;
        domain = "localhost";
        protocol = "http";
      };
    };
  };

  programs.ssh = {
    extraConfig = ''
      Host *
        IdentityAgent ~/.1password/agent.sock
    '';
  };

  #Don't change these
  system.stateVersion = "23.11";
  nix.settings.experimental-features = [
    "nix-command"
    "flakes"
  ];
}
