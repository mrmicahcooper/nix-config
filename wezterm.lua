local wezterm = require 'wezterm'
return {
  enable_wayland = false,
  color_scheme = "Catppuccin Mocha",
  -- font = wezterm.font 'Maple Mono NF',
  font = wezterm.font '0xProto Nerd Font Mono',
  font_size = 19,
  line_height = 1.2,
  dpi = 96.0,
  window_decorations = "RESIZE",
  window_padding = {
    left = 0,
    right = 0,
    top = 0,
    bottom = 0
  }
}
