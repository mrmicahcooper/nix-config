{
  add-untracked = "!git status --porcelain | awk '/\\?\\?/{ print $2 }' | xargs git add";
  aliases = "!git config -l | grep '^alias' | cut -c 7- | sort";
  b = "branch";
  br = "branch";
  cheddar = "commit --amend -CHEAD";
  co = "checkout";
  doff = "reset HEAD^";
  fixup = "commit --fixup";
  mush = "push -o merge_request.create -o merge_request.target=master -o merge_request.merge_when_pipeline_succeeds";
  pruneremote = "remote prune origin";
  ri = "rebase --interactive";
  tr = "log --graph --oneline --decorate --color";
  tree = "log --graph --oneline --decorate --color --all";
}
