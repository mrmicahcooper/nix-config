{
  enable = true;
  arguments = [
    "--colors=line:fg:yellow"
    "--colors=line:style:bold"
    "--colors=match:bg:yellow"
    "--colors=match:fg:black"
    "--colors=match:style:nobold"
    "--colors=path:fg:green"
    "--colors=path:style:bold"
    "--hidden"
    "--ignore"
    "--max-columns-preview"
    "--max-columns=150"
    "--smart-case"
  ];
}
