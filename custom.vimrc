let g:projectionist_heuristics = {
    \ "ember-cli-build.js": {
    \   "app/router.js": {"type": "router"},
    \   "app/routes/": {"type": "route"},
    \   "app/routes/*.js": {
    \    "type": "route",
    \    "related": "app/controllers/{}.js",
    \    "alternate": "app/controllers/{}.js",
    \    "template": [
    \      "import Route from '@ember/routing/route';",
    \      "",
    \      "export default class {basename|capitalize}Route extends Route {open}{close}",
    \    ]
    \   },
    \   "README.md": {"type": "readme"},
    \   "ember-cli-build.js": {"type": "build"},
    \   "config/environment.js": {"type": "env"},
    \   "app/controllers/": {"type": "controller"},
    \   "app/controllers/*.js": {
    \     "type": "controller",
    \     "alternate": "app/templates/{}.hbs",
    \     "template": [
    \      "import Controller from '@ember/controller';",
    \      "",
    \      "export default class {basename|capitalize}Controller extends Controller {open}{close}",
    \    ]
    \   },
    \   "app/templates/": {"type":"templates"},
    \   "app/templates/*.hbs": {
    \    "type":"template",
    \    "alternate":"app/controllers/{}.js"
    \   },
    \   "app/templates/components/": {"type":"ctemplates"},
    \   "app/templates/components/*.hbs": {
    \     "type": "ctemplate",
    \     "alternate": "app/components/{}.js"
    \   },
    \   "app/components/*.hbs": {
    \     "type": "ctemplate",
    \     "alternate": "app/components/{}.js"
    \   },
    \   "app/components": {"type":"pods"},
    \   "app/components/*.js": {
    \     "type":"component",
    \     "alternate": [
    \       "app/templates/components/{}.hbs",
    \       "app/components/{}.hbs",
    \       "app/components/{}/template.hbs"
    \     ],
    \     "template": [
    \      "import Component from '@glimmer/component';",
    \      "",
    \      "export default class {basename|capitalize}Component extends Component {open}{close}",
    \    ]
    \   },
    \   "app/services": {"type":"services"},
    \   "app/services/*.js": {
    \     "type":"services",
    \     "template": [
    \      "import Service, {open} attr {close}  from '@ember/service';",
    \      "",
    \      "export default class {basename|capitalize} extends Service {open}{close}",
    \    ]
    \   },
    \  },
    \ "app/models/": {
    \   "app/models": {"type":"model"},
    \   "app/models/*.js": {
    \     "type":"model",
    \     "template": [
    \      "import Model, {open} attr {close}  from '@ember-data/model';",
    \      "",
    \      "export default class {basename|capitalize} extends Model {open}{close}",
    \     ],
    \     "alternate": "mirage/factories/{}.js"
    \   },
    \ },
    \ "app/adapters/": {
    \   "app/adapters": {"type":"adapter"},
    \   "app/adapters/*.js": {
    \     "type":"adapter"
    \   }
    \ },
    \ "app/serializers/": {
    \   "app/serializers": {"type":"serializer"},
    \   "app/serializers/*.js": {
    \     "type":"serializer"
    \   }
    \ },
    \ "mix.exs": {
    \   "README.md": {"type": "readme"},
    \   "lib/*.ex": {"type": "lib", "alternate": "test/{}_test.exs"},
    \   "test/": {"type": "test"},
    \   "test/support/*.ex": {"type": "support"},
    \   "test/support": {"type": "support"},
    \   "test/*_test.exs": {"type": "test", "alternate": "lib/{}.ex"},
    \   "lib/**/controllers/*_controller.ex": {
    \     "type": "controller",
    \     "template": [
    \       "defmodule {camelcase|capitalize|dot}Controller do",
    \       "  use {dirname|camelcase|capitalize}, :controller",
    \       "",
    \       "end"
    \     ]
    \   },
    \   "lib/**/channels/*_channel.ex": {"type": "channel"},
    \   "lib/**/templates/": {"type": "template"},
    \   "lib/**/templates/layout/": {"type": "layout"},
    \   "lib/**/templates/*.html.heex": {"type": "template"},
    \   "lib/**/templates/layout/*.html.heex": {"type": "layout"},
    \   "lib/**/views/": { "type":"view" },
    \   "lib/**/views/*_view.ex": {
    \     "type": "view",
    \     "template": [
    \       "defmodule {camelcase|capitalize|dot}View do",
    \       "  use {dirname|camelcase|capitalize}, :view",
    \       "",
    \       "end"
    \     ]
    \   },
    \   "lib/**/plugs/*.ex": {"type": "plug"},
    \   "lib/**/plugs/": {"type": "plug"},
    \   "lib/**_web/*.ex": {"type": "web"},
    \   "lib/**/endpoint.ex": {"type": "endpoint"},
    \   "config/config.exs": {"type": "fig"},
    \   "config/*.exs": {"type": "fig"},
    \   "priv/repo/migrations/": {"type": "migration"},
    \   "priv/repo/migrations/*.exs": {"type": "migration"},
    \  },
    \ "mirage/&ember-cli-build.js": {
    \   "mirage/factories/*.js": {
    \     "type":"factory",
    \     "template": [
    \       "import {open} Factory {close} from 'ember-cli-mirage';",
    \       "",
    \       "export default Factory.extend({open}{close});"
    \     ],
    \     "related": "app/models/{}.js"
    \   },
    \   "mirage/*.js": {"type":"mirage"}
    \ },
    \ "app/gql/": {
    \  "app/gql/queries/*.qraphql": {"type": "query"},
    \  "app/gql/mutations/*.qraphql": {"type": "mutation"}
    \ },
    \ "src/commands/": {
    \  "src/commands/": {"type": "command"},
    \  "src/commands/*.ts": {"type": "command"}
    \ },
    \ "jest.config.js": {
    \   "src/*.ts": {
    \     "type":"lib",
    \     "alternate": "src/__tests__/{}.test.ts"
    \   },
    \   "src/__tests__/*.test.ts": {
    \     "type":"test",
    \     "alternate": "src/{}.ts"
    \   }
    \ },
    \ "src/extension.ts": { 
    \   "src/*.ts": {
    \     "type":"lib",
    \     "alternate": "src/test/{}.test.ts"
    \   },
    \   "src/test/*.test.ts": {
    \     "type":"test",
    \     "alternate": "src/{}.ts"
    \   }
    \ }
    \}

" Vim Testing
function! NvimTest(command) abort
  let jobid = get(g:, 'nvimtest_job_id', 0)
  " let test#neovim#term_position = "vertical topleft"
  if jobid
    call chansend(jobid, ['clear', a:command, ''])
  else
    let term_position = get(g:, 'test#neovim#term_position', 'vertical topleft')
    execute term_position . ' new'
    terminal
    setlocal nonumber
    startinsert
    execute "file! vim-test-window"
    vertical-resize 90
    let g:nvimtest_job_id = b:terminal_job_id
    call chansend(b:terminal_job_id, [a:command, ''])
    au BufDelete <buffer> let g:nvimtest_job_id = 0
    tmap <buffer> <Esc> <C-\><C-n>
    tmap <buffer> <C-o> <C-\><C-n>
    tmap <buffer> <C-w> <C-\><C-n><C-w>
    let b:neoterm_autoscroll = 1
    stopinsert
    wincmd p
  endif
endfunction

let g:test#custom_strategies = {'nvimtest': function('NvimTest')}
let g:test#strategy = 'nvimtest'
